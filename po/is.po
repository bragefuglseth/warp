# Icelandic translation for warp.
# Copyright (C) 2023 warp's COPYRIGHT HOLDER
# This file is distributed under the same license as the warp package.
#
# Ingirafn <ingirafn@this.is>, 2023, 2024.
# Sveinn í Felli <sv1@fellsnet.is>, 2023, 2024.
msgid ""
msgstr ""
"Project-Id-Version: warp main\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/World/warp/issues\n"
"POT-Creation-Date: 2024-04-12 16:03+0000\n"
"PO-Revision-Date: 2024-04-17 08:13+0000\n"
"Last-Translator: Sveinn í Felli <sv1@fellsnet.is>\n"
"Language-Team: Icelandic\n"
"Language: is\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n % 100 != 1 && n % 100 != 21 && n % 100 !="
" 31 && n % 100 != 41 && n % 100 != 51 && n % 100 != 61 && n % 100 != 71 && n "
"% 100 != 81 && n % 100 != 91);\n"
"X-Generator: Lokalize 22.04.3\n"

#: data/app.drey.Warp.desktop.in.in:3 data/app.drey.Warp.metainfo.xml.in.in:4
#: src/main.rs:122 src/ui/window.ui:81
msgid "Warp"
msgstr "Warp"

#: data/app.drey.Warp.desktop.in.in:4 data/app.drey.Warp.metainfo.xml.in.in:5
#: src/ui/welcome_dialog.ui:24
msgid "Fast and secure file transfer"
msgstr "Hraður og öruggur skráaflutningur"

#: data/app.drey.Warp.desktop.in.in:9
msgid "Gnome;GTK;Wormhole;Magic-Wormhole;"
msgstr "Gnome;GTK;Wormhole;Magic-Wormhole;"

#. developer_name tag deprecated with Appstream 1.0
#: data/app.drey.Warp.metainfo.xml.in.in:9 src/ui/application.rs:233
msgid "Fina Wilke"
msgstr "Fina Wilke"

#: data/app.drey.Warp.metainfo.xml.in.in:43
msgid ""
"Warp allows you to securely send files to each other via the internet or "
"local network by exchanging a word-based code."
msgstr ""
"Með Warp getur fólk sent skrár milli hvors annars á öruggan hátt "
"yfirinternetið eða á innra neti með því að skiptast á orðakóða."

#: data/app.drey.Warp.metainfo.xml.in.in:47
msgid ""
"The best transfer method will be determined using the “Magic Wormhole” "
"protocol which includes local network transfer if possible."
msgstr ""
"Besta fluttninsaðferðin ákvarðast af \"Magic Wormhole” samskiptamátanum sem "
"getur notast við sendingar á innra neti ef hægt er."

#: data/app.drey.Warp.metainfo.xml.in.in:51
msgid "Features"
msgstr "Eiginleikar"

#: data/app.drey.Warp.metainfo.xml.in.in:53
msgid "Send files between multiple devices"
msgstr "Senda skrár milli margra tækja"

#: data/app.drey.Warp.metainfo.xml.in.in:54
msgid "Every file transfer is encrypted"
msgstr "Allur skráaflutningur er dulritaður"

#: data/app.drey.Warp.metainfo.xml.in.in:55
msgid "Directly transfer files on the local network if possible"
msgstr "Senda skrár beint á milli á innra neti ef hægt er"

#: data/app.drey.Warp.metainfo.xml.in.in:56
msgid "An internet connection is required"
msgstr "Internettenging er nauðsynleg"

#: data/app.drey.Warp.metainfo.xml.in.in:57
msgid "QR Code support"
msgstr "Stuðningur við QR-kóða"

#: data/app.drey.Warp.metainfo.xml.in.in:58
msgid ""
"Compatibility with the Magic Wormhole command line client and all other "
"compatible apps"
msgstr ""
"Samhæfni við skipanalínu Magic Wormhole og annarra sambærilegra forrita"

#: data/app.drey.Warp.metainfo.xml.in.in:64
#: data/app.drey.Warp.metainfo.xml.in.in:84
msgid "Main Window"
msgstr "Aðalgluggi"

#. Translators: Entry placeholder; This is a noun
#: data/app.drey.Warp.metainfo.xml.in.in:68
#: data/app.drey.Warp.metainfo.xml.in.in:88 src/ui/window.ui:199
msgid "Transmit Code"
msgstr "Sendingakóði"

#: data/app.drey.Warp.metainfo.xml.in.in:72
#: data/app.drey.Warp.metainfo.xml.in.in:92
msgid "Accept File Transfer"
msgstr "Samþykkja skráaflutning"

#: data/app.drey.Warp.metainfo.xml.in.in:76
#: data/app.drey.Warp.metainfo.xml.in.in:96 src/ui/action_view.rs:647
msgid "Receiving File"
msgstr "Tek við skrá"

#: data/app.drey.Warp.metainfo.xml.in.in:80
#: data/app.drey.Warp.metainfo.xml.in.in:100 src/ui/action_view.rs:656
msgid "File Transfer Complete"
msgstr "Skráaflutningi lokið"

#: data/resources/ui/help_overlay.ui:11
msgctxt "shortcut window"
msgid "General"
msgstr "Almennt"

#: data/resources/ui/help_overlay.ui:14
msgctxt "shortcut window"
msgid "Show Help"
msgstr "Sýna hjálp"

#: data/resources/ui/help_overlay.ui:20
msgctxt "shortcut window"
msgid "Show Shortcuts"
msgstr "Sýna flýtilykla"

#: data/resources/ui/help_overlay.ui:26
msgctxt "shortcut window"
msgid "Show Preferences"
msgstr "Sýna kjörstillingar"

#: data/resources/ui/help_overlay.ui:32
msgctxt "shortcut window"
msgid "Quit"
msgstr "Hætta"

#: data/resources/ui/help_overlay.ui:40
msgctxt "shortcut window"
msgid "File Transfer"
msgstr "Skráaflutningur"

#: data/resources/ui/help_overlay.ui:43
msgctxt "shortcut window"
msgid "Send File"
msgstr "Senda skrá"

#: data/resources/ui/help_overlay.ui:49
msgctxt "shortcut window"
msgid "Send Folder"
msgstr "Senda möppu"

#: data/resources/ui/help_overlay.ui:55
msgctxt "shortcut window"
msgid "Receive File"
msgstr "Taka við skrá"

#. Translators: {0} = file size transferred, {1} = total file size, Example: 17.3MB / 20.5MB
#: src/gettext/duration.rs:11
msgctxt "File size transferred"
msgid "{0} / {1}"
msgstr "{0} / {1}"

#. Translators: File transfer time left
#: src/gettext/duration.rs:18
msgid "One second left"
msgid_plural "{} seconds left"
msgstr[0] "Ein sekúnda eftir"
msgstr[1] "{} sekúndur eftir"

#. Translators: File transfer time left
#: src/gettext/duration.rs:26
msgid "One minute left"
msgid_plural "{} minutes left"
msgstr[0] "Ein mínúta eftir"
msgstr[1] "{} mínútur eftir"

#. Translators: File transfer time left
#: src/gettext/duration.rs:34
msgid "One hour left"
msgid_plural "{} hours left"
msgstr[0] "Ein klukkustund eftir"
msgstr[1] "{} klukkustundir eftir"

#. Translators: File transfer time left
#: src/gettext/duration.rs:42
msgid "One day left"
msgid_plural "{} days left"
msgstr[0] "Einn dagur eftir"
msgstr[1] "{} dagar eftir"

#. Translators: {0} = 11.3MB / 20.7MB, {1} = 3 seconds left
#: src/gettext/duration.rs:52
msgctxt "Combine bytes progress {0} and time remaining {1}"
msgid "{0} — {1}"
msgstr "{0} - {1}"

#. Translators: Notification when clicking on "Copy Code to Clipboard" button
#: src/ui/action_view.rs:257
msgid "Copied Code to Clipboard"
msgstr "Afritaði kóða á klippispjald"

#. Translators: Notification when clicking on "Copy Link to Clipboard" button
#: src/ui/action_view.rs:274
msgid "Copied Link to Clipboard"
msgstr "Afritaði tengil á klippispjald"

#: src/ui/action_view.rs:288
msgid "Copied Error to Clipboard"
msgstr "Afritaði villuboð á klippispjald"

#: src/ui/action_view.rs:290
msgid "No error available"
msgstr "Engin villa tiltæk"

#: src/ui/action_view.rs:455
msgid "Creating Archive"
msgstr "Bý til skráasafn"

#: src/ui/action_view.rs:459
msgid "Compressing folder “{}”"
msgstr "Þjappa möppu “{}”"

#. Translators: Title
#: src/ui/action_view.rs:475 src/ui/action_view.rs:538
msgid "Connecting"
msgstr "Tengist"

#. Translators: Description, Filename
#: src/ui/action_view.rs:478
msgid "Requesting file transfer"
msgstr "Bið um skráaflutning"

#. Translators: Description, argument is filename
#: src/ui/action_view.rs:499
msgid "Ready to send “{}”."
msgstr "Tilbúið að senda “{}”."

#. Translators: Help dialog line 1, Code words and QR code visible,
#: src/ui/action_view.rs:504
msgid ""
"The receiver needs to enter or scan this code to begin the file transfer."
msgstr ""
"Viðtakandi þarf að færa inn eða skanna þennan kóða til að hefja "
"skráaflutning."

#: src/ui/action_view.rs:508
msgid "The QR code is compatible with the following apps: {}."
msgstr "QR-kóðinn er samhæfður við eftirfarandi forrit: {}."

#: src/ui/action_view.rs:516
msgid ""
"You have entered a custom rendezvous server URL in preferences. Please "
"verify the receiver also uses the same rendezvous server."
msgstr ""
"Þú hefur sett inn sérsniðið netfang rendezvous-netþjóns í kjörstillingum. "
"Gakktu úr skugga um að móttakandinn noti sama rendezvous-netþjón."

#: src/ui/action_view.rs:521
msgid "Click the QR code to copy the link to the clipboard."
msgstr "Smelltu á QR kóðann til að afrita hann á klippispjaldið."

#. Translators: Description, Transfer Code
#: src/ui/action_view.rs:541
msgid "Connecting to peer with code “{}”"
msgstr "Tengist jafningja með kóða “{}”"

#: src/ui/action_view.rs:551
msgid "Connected to Peer"
msgstr "Tengdur jafningja"

#. Translators: Description
#: src/ui/action_view.rs:560
msgid "Preparing to send file"
msgstr "Undirbý sendingu skráar"

#. Translators: Description
#: src/ui/action_view.rs:567
msgid "Preparing to receive file"
msgstr "Undirbý móttöku skráar"

#. Translators: File receive confirmation message dialog; Filename, File size
#: src/ui/action_view.rs:581
msgid ""
"Your peer wants to send you “{0}” (Size: {1}).\n"
"Do you want to download this file? The default action will save the file to "
"your Downloads folder."
msgstr ""
"Jafningi þinn vill senda þér skrá “{0}” (stærð: {1}).\n"
"Vilt þú sækja þessa skrá? Sjálfgefið vistast hún í niðurhalsmöppuna þína"

#: src/ui/action_view.rs:586
msgid "Ready to Receive File"
msgstr "Tilbúið að taka við skrá"

#: src/ui/action_view.rs:588
msgid ""
"A file is ready to be transferred. The transfer needs to be acknowledged."
msgstr "Skrá er tilbúin til flutnings. Flutninginn þarf að samþykkja."

#. Translators: Description, During transfer
#: src/ui/action_view.rs:618
msgid "File “{}” via local network direct transfer"
msgstr "Skrá \"{}” flutt beint um innra net"

#. Translators: Description, During transfer
#: src/ui/action_view.rs:621
msgid "File “{}” via direct transfer"
msgstr "Skrá \"{}” með beinni skráasendingu"

#. Translators: Description, During transfer
#: src/ui/action_view.rs:627
msgid "File “{0}” via relay {1}"
msgstr "Skrá “{0}” gegnum endurvarpa {1}"

#. Translators: Description, During transfer
#: src/ui/action_view.rs:630
msgid "File “{}” via relay"
msgstr "Skrá “{}” gegnum endurvarpa"

#. Translators: Description, During transfer
#: src/ui/action_view.rs:634
msgid "File “{}” via Unknown connection method"
msgstr "Skrá “{}” gegnum óþekkta tengiaðferð"

#. Translators: Title
#: src/ui/action_view.rs:641
msgid "Sending File"
msgstr "Sendi skrá"

#. Translators: Description, Filename
#: src/ui/action_view.rs:664
msgid "Successfully sent file “{}”"
msgstr "Tókst að senda skrá “{}”"

#. Translators: Filename
#: src/ui/action_view.rs:682
msgid "File has been saved to the selected folder as “{}”"
msgstr "Skráin var vistuð í tilgreinda möppu sem “{}”"

#. Translators: Filename
#: src/ui/action_view.rs:689
msgid "File has been saved to the Downloads folder as “{}”"
msgstr "Skráin  var vistuð í niðurhalsmöppuna sem “{}”"

#. Translators: Title
#: src/ui/action_view.rs:715 src/ui/action_view.ui:288
msgid "File Transfer Failed"
msgstr "Skráaflutningur misheppnaðist"

#: src/ui/action_view.rs:717
msgid "The file transfer failed: {}"
msgstr "Skráaflutningurinn misheppnaðist: {}"

#. Translators: When opening a file
#: src/ui/action_view.rs:831
msgid "Specified file / directory does not exist"
msgstr "Tilgreind skrá / mappa er ekki til"

#: src/ui/action_view.rs:853
msgid ""
"Error parsing rendezvous server URL. An invalid URL was entered in the "
"settings."
msgstr ""
"Villa við þáttun slóðar rendezvous-netþjóns . Ógild slóð var sett inn í "
"stillingarnar."

#: src/ui/action_view.rs:860
msgid "Error parsing transit URL. An invalid URL was entered in the settings."
msgstr ""
"Villa við þáttun millifærsluslóðar . Ógild slóð var sett inn í stillingarnar."

#: src/ui/action_view.rs:949
msgid "Invalid path selected: {}"
msgstr "Ógild slóð valin: {}"

#. Translators: Above progress bar for creating an archive to send as a folder
#: src/ui/action_view.rs:1196
msgid "{} File - Size: {}"
msgid_plural "{} Files - Size: {}"
msgstr[0] "{} Skrá - Stærð: {}"
msgstr[1] "{} Skrár - Stærð: {}"

#: src/ui/action_view.ui:4
msgid "Save As"
msgstr "Vista sem"

#. Translators: Button; Transmit Link is a noun
#: src/ui/action_view.ui:108
msgid "Copy Transmit Link"
msgstr "Afrita millifærslutengil"

#: src/ui/action_view.ui:133
msgid "Your Transmit Code"
msgstr "Sendingakóðinn þinn"

#. Translators: Button; Transmit Code is a noun
#: src/ui/action_view.ui:181
msgid "Copy Transmit Code"
msgstr "Afrita sendingarkóða"

#. Translators: Title
#: src/ui/action_view.ui:198
msgid "Accept File Transfer?"
msgstr "Taka við skráaflutningi?"

#: src/ui/action_view.ui:207
msgid "Save to Downloads Folder"
msgstr "Vista í niðurhalsmöppuna"

#. Translators: Button
#: src/ui/action_view.ui:209
msgid "_Accept"
msgstr "T_aka við"

#. Translators: Button
#: src/ui/action_view.ui:223
msgid "Sa_ve As…"
msgstr "Vista sem..."

#. Translators: Title
#: src/ui/action_view.ui:243
msgid "File Transfer Successful"
msgstr "Skráaflutningur heppnaðist"

#. Translators: Button
#: src/ui/action_view.ui:252
msgid "_Open File"
msgstr "_Opna skrá"

#. Translators: Button
#: src/ui/action_view.ui:267 src/ui/window.ui:51
msgid "_Show in Folder"
msgstr "_Birta í möppu"

#. Translators: Button
#: src/ui/action_view.ui:293
msgid "Co_py Error Message"
msgstr "_Afrita villuboð"

#. Translators: Button
#: src/ui/action_view.ui:309
msgid "_Cancel"
msgstr "_Hætta við"

#: src/ui/application.rs:63
msgid "Unable to use transfer link: another transfer already in progress"
msgstr "Ekki hægt að nota flutningstengil: annar flutningur þegar í gangi"

#: src/ui/application.rs:234
msgid "Tobias Bernard"
msgstr "Tobias Bernard"

#: src/ui/application.rs:234
msgid "Sophie Herold"
msgstr "Sophie Herold"

#: src/ui/application.rs:235
msgid "translator-credits"
msgstr "Ingirafn, ingirafn at this dot is, 2023"

#: src/ui/application.rs:250
msgid "Sending a File"
msgstr "Sendi skrá"

#: src/ui/application.rs:251
msgid "Receiving a File"
msgstr "Tek við skrá"

#: src/ui/camera.rs:214 src/ui/camera.rs:331
msgid ""
"Camera access denied. Open Settings and allow Warp to access the camera."
msgstr ""
"Aðgangur að myndavél lokaður. Opnaðu stillingar og leyfðu Warp aðgang að "
"myndavélinni."

#: src/ui/camera.rs:218
msgid "Failed to start the camera: {}"
msgstr "Tókst ekki að kveikja á myndavél: {} "

#: src/ui/camera.rs:221
msgid "Error"
msgstr "Villa"

#: src/ui/camera.rs:232
msgid "Error talking to the camera portal"
msgstr "Villa við samskipti við myndavéla gátt"

#: src/ui/camera.rs:461
msgid "Could not use the camera portal: {}"
msgstr "Gat ekki notað myndavélagátt: {}"

#: src/ui/camera.rs:468
msgid "Could not start the device provider: {}"
msgstr "Gat ekki ræst hjálpar forrit : {}"

#: src/ui/camera.rs:499
msgid "No Camera Found"
msgstr "Engin myndavél fannst"

#: src/ui/camera.rs:501
msgid "Connect a camera to scan QR codes"
msgstr "Tengdu myndavél til að skanna QR kóða"

#: src/ui/camera.ui:11 src/ui/window.rs:106
msgid "Scan QR Code"
msgstr "Skanna QR kóða"

#: src/ui/camera.ui:49
msgid "_Retry"
msgstr "_Reyna aftur"

#: src/ui/camera.ui:60
msgid "_Troubleshooting"
msgstr "_Villanagreining"

#: src/ui/fs.rs:16
msgid "Downloads dir missing. Please set XDG_DOWNLOAD_DIR"
msgstr "Niðurhalsmappa finnst ekki. Stilltu XDG_DOWNLOAD_DIR"

#: src/ui/preferences.rs:97
msgid ""
"Changing the rendezvous server URL needs to be done on both sides of the "
"transfer. Only enter a server URL you can trust.\n"
"\n"
"Leaving these entries empty will use the app defaults:\n"
"Rendezvous Server: “{0}”\n"
"Transit Server: “{1}”"
msgstr ""
"Báðir aðilar þurfa að breyta slóð rendezvous-netþjónsins. Notaðu einungis "
"slóð netþjóns sem þú treystir.\n"
"\n"
"Ef engin netþjónn er valin er notuð sjálfgefin slóð forritsins:\n"
"Rendezvous-netþjónn: “{0}”\n"
"Sendi-netþjónn “{1}”"

#: src/ui/preferences.ui:4
msgid "Preferences"
msgstr "Kjörstillingar"

#: src/ui/preferences.ui:8
msgid "Network"
msgstr "Netkerfi"

#: src/ui/preferences.ui:12
msgid "Code Words"
msgstr "Orð kóðans"

#: src/ui/preferences.ui:13
msgid ""
"The code word count determines the security of the transfer.\n"
"\n"
"A short code is easy to remember but increases the risk of someone else "
"guessing the code. As a code may only be guessed once, the risk is very "
"small even with short codes. A length of 4 is very secure."
msgstr ""
"Fjöldi orða í kóða hefur áhrif á öryggi sendingar.\n"
"\n"
"Stuttan kóða er auðvelt að muna en þá aukast líkurnar á að einhver annar "
"geti giskað á kóðann.  Þar sem aðeins er hægt að giska á kóða einu sinni, "
"eru líkurnar mjög litlar jafnvel fyrir stutta kóða. Fjögura stafa kóði er "
"mjög öruggur."

#: src/ui/preferences.ui:18
msgid "Code Word Count"
msgstr "Fjöldi orða í kóða"

#: src/ui/preferences.ui:26
msgid "Server URLs"
msgstr "Slóðir netþjóna"

#: src/ui/preferences.ui:29
msgid "Rendezvous Server URL"
msgstr "Slóð rendezvous-netþjóns"

#: src/ui/preferences.ui:36
msgid "Transit Server URL"
msgstr "Slóð sendi-netþjóns"

#: src/ui/welcome_dialog.ui:10
msgid "Welcome"
msgstr "Velkomin"

#: src/ui/welcome_dialog.ui:23
msgid "Welcome to Warp"
msgstr "Velkomin í Warp"

#. Translators: Button
#: src/ui/welcome_dialog.ui:31
msgid "Next"
msgstr "Næsta"

#: src/ui/welcome_dialog.ui:79
msgid "Introduction"
msgstr "Kynning"

#: src/ui/welcome_dialog.ui:90
msgid ""
"Warp makes file transfer simple. To get started, both parties need to "
"install Warp on their devices.\n"
"\n"
"After selecting a file to transmit, the sender needs to tell the receiver "
"the displayed transmit code. This is preferably done via a secure "
"communication channel.\n"
"\n"
"When the receiver has entered the code, the file transfer can begin.\n"
"\n"
"For more information about Warp, open the Help section from the Main Menu."
msgstr ""
"Warp auðveldar skráaflutning. Í fyrstu þurfa báðir aðilar að setja upp Warp "
"á tölvurnar sínar.\n"
"\n"
"Eftir að hafa valið skrá til að senda, þarf sendandinn að senda móttakanda "
"gefin sendingarkóða. Helst í gegnum öruggan samskiptamáta.\n"
"\n"
"Þegar móttakandi setur inn kóðann, getur skráaflutningurinn hafist.\n"
"\n"
"Til að sjá ítarlegri upplýsingar um Warp, skaltu opna Hjálp í "
"aðalvalmyndinni."

#. Translators: Big button to finish welcome screen
#: src/ui/welcome_dialog.ui:103
msgid "Get Started Using Warp"
msgstr "Byrjaðu að nota Warp"

#: src/ui/window.rs:129
msgid ""
"Error loading config file “{0}”, using default config.\n"
"Error: {1}"
msgstr ""
"Villa við að hlaða inn stillingaskrá “{0}”, nota sjálfgefnar stillingar .\n"
"Villa: {1}"

#: src/ui/window.rs:235
msgid "Error saving configuration file: {}"
msgstr "Villa við að vista stillingaskrá: {}"

#: src/ui/window.rs:341
msgid "Select File to Send"
msgstr "Veldu skrá til að senda"

#: src/ui/window.rs:349
msgid "Select Folder to Send"
msgstr "Veldu möppu til að senda"

#: src/ui/window.rs:406
msgid "“{}” appears to be an invalid Transmit Code. Please try again."
msgstr "“{}” virðist vera rangur sendigakóði. Prófaðu aftur."

#: src/ui/window.rs:527
msgid "Sending files with a preconfigured code is not yet supported"
msgstr "Að senda skrá með fyrirfram tilbúnum kóða er enn ekki hægt"

#. Translators: menu item
#: src/ui/window.ui:7
msgid "_Preferences"
msgstr "K_jörstillingar"

#. Translators: menu item
#: src/ui/window.ui:12
msgid "_Keyboard Shortcuts"
msgstr "_Flýtilyklar"

#. Translators: menu item
#: src/ui/window.ui:17
msgid "_Help"
msgstr "_Hjálp"

#. Translators: menu item
#: src/ui/window.ui:22
msgid "_About Warp"
msgstr "_Um Warp"

#. Translators: Notification when code was automatically detected in clipboard and inserted into code entry on receive page
#: src/ui/window.ui:33
msgid "Inserted code from clipboard"
msgstr "Setti inn kóða af klippispjaldi"

#. Translators: File receive confirmation message dialog title
#: src/ui/window.ui:38
msgid "Abort File Transfer?"
msgstr "Hætta við skráaflutning?"

#: src/ui/window.ui:39
msgid "Do you want to abort the current file transfer?"
msgstr "Vilt þú hætta við þennan skráaflutning?"

#: src/ui/window.ui:42
msgid "_Continue"
msgstr "_Halda áfram"

#: src/ui/window.ui:43
msgid "_Abort"
msgstr "_Hætta við"

#. Translators: Error dialog title
#: src/ui/window.ui:48
msgid "Unable to Open File"
msgstr "Gat ekki opnað skrá"

#: src/ui/window.ui:50 src/util/error.rs:191
msgid "_Close"
msgstr "_Loka"

#: src/ui/window.ui:98
msgid "Main Menu"
msgstr "Aðalvalmynd"

#: src/ui/window.ui:117
msgid "_Send"
msgstr "_Senda"

#: src/ui/window.ui:129
msgid "Send File"
msgstr "Senda skrá"

#: src/ui/window.ui:130
msgid "Select or drop the file or directory to send"
msgstr "Veldu eða slepptu skrá / möppu til að senda"

#. Translators: Button
#: src/ui/window.ui:141
msgid "Select _File…"
msgstr "V_elja skrá…"

#. Translators: Button
#: src/ui/window.ui:155
msgid "Select F_older…"
msgstr "Velja _möppu..."

#: src/ui/window.ui:175
msgid "_Receive"
msgstr "_Taka við"

#: src/ui/window.ui:184
msgid "Receive File"
msgstr "Taka við skrá"

#. Translators: Text above code input box, transmit code is a noun
#: src/ui/window.ui:186
msgid "Enter the transmit code from the sender"
msgstr "Færið inn sendingakóðann frá sendanda"

#. Translators: Button
#: src/ui/window.ui:221
msgid "Receive _File"
msgstr "Taka við _skrá"

#: src/ui/window.ui:247
msgid "File Transfer"
msgstr "Skráaflutningur"

#: src/util.rs:33 src/util.rs:38
msgid "Failed to open downloads folder."
msgstr "Tókst ekki að opna niðurhalsmöppuna."

#: src/util.rs:205 src/util.rs:278
msgid "The URI format is invalid"
msgstr "URI-sniðið er ógilt"

#: src/util.rs:209 src/util.rs:213
msgid "The code does not match the required format"
msgstr "Kóðinn passar ekki við umbeðið snið"

#: src/util.rs:228 src/util.rs:234
msgid "Unknown URI version: {}"
msgstr "Óþekkt URI útgáfa: {}"

#: src/util.rs:243
msgid "The URI parameter “rendezvous” contains an invalid URL: “{}”"
msgstr "URI-sniðið “rendezvous” inniheldur ógilda slóð: “{}”"

#: src/util.rs:255
msgid "The URI parameter “role” must be “follower” or “leader” (was: “{}”)"
msgstr ""
"URI-breytan \"role\" verður að vera \"follower\" eða \"leader\" (var: “{}”)"

#: src/util.rs:262
msgid "Unknown URI parameter “{}”"
msgstr "Óþekkt URI-breyta “{}”"

#: src/util/error.rs:172
msgid "An error occurred"
msgstr "Villa kom upp"

#: src/util/error.rs:202 src/util/error.rs:285
msgid "Corrupt or unexpected message received"
msgstr "Skemmd eða óvænt skilaboð móttekin"

#: src/util/error.rs:207
msgid ""
"The rendezvous server will not allow further connections for this code. A "
"new code needs to be generated."
msgstr ""
"Rendezvous-netþjónninn leyfir ekki fleiri tengingar fyrir þennan kóða. Það "
"þarf að búa til nýjan kóða."

#: src/util/error.rs:209
msgid ""
"The rendezvous server removed the code due to inactivity. A new code needs "
"to be generated."
msgstr ""
"Rendezvous-netþjónninn fjarlægði kóðann þar sem beðið var of lengi með að "
"nota hann. Það þarf að búa til nýjan kóða."

#: src/util/error.rs:211
msgid "The rendezvous server responded with an unknown message: {}"
msgstr "Rendezvous netþjónninn svaraði með óþekktum skilaboðum: {}"

#: src/util/error.rs:214
msgid ""
"Error connecting to the rendezvous server.\n"
"You have entered a custom rendezvous server URL in preferences. Please "
"verify the URL is correct and the server is working."
msgstr ""
"Villa við að tengjast rendezvous-netþjóninum.\n"
"Þú hefur sett inn sérsniðna slóð rendezvous-netþjóns í kjörstillingum. "
"Vinsamlegast gangið úr skugga um að slóðin sé rétt og að netþjónninn sé í "
"gangi."

#: src/util/error.rs:216
msgid ""
"Error connecting to the rendezvous server.\n"
"Please try again later / verify you are connected to the internet."
msgstr ""
"Villa við að tengjast rendezvous-netþjóninum.\n"
"Reyndu aftur síðar / gakktu úr skugga um að þú sért tengdur internetinu."

#: src/util/error.rs:220
msgid ""
"Encryption key confirmation failed. If you or your peer didn't mistype the "
"code, this is a sign of an attacker guessing passwords. Please try again "
"some time later."
msgstr ""
"Tókst ekki að staðfesta dulritunarlykil. Ef mótaðili þinn setti ekki "
"aðgangsorðið rangt inn, getur það bent það til að nethrappur sé að reyna að "
"giska á aðgangsorðið. Reyndu aftur síðar."

#: src/util/error.rs:222
msgid "Cannot decrypt a received message"
msgstr "Get ekki afdulkóðað móttekin skilaboð"

#: src/util/error.rs:223 src/util/error.rs:296 src/util/error.rs:302
msgid "An unknown error occurred"
msgstr "Óþekkt villa átti sér stað"

#: src/util/error.rs:229
msgid "File / Directory not found"
msgstr "Skrá / mappa fannst ekki"

#: src/util/error.rs:230
msgid "Permission denied"
msgstr "Heimild hafnað"

#: src/util/error.rs:237
msgid "Canceled"
msgstr "Hætt við"

#: src/util/error.rs:239
msgid "Portal service response: Permission denied"
msgstr "Gáttar villa: Aðgangsheimild hafnað"

#: src/util/error.rs:242
msgid "The portal service has failed: {}"
msgstr "Gáttarþjónusta brást: {}"

#: src/util/error.rs:245
msgid "Error communicating with the portal service via zbus: {}"
msgstr "Villa í samskiptum við gegnum zbus: {}"

#: src/util/error.rs:248 src/util/error.rs:249
msgid "Portal error: {}"
msgstr "Villa í gátt : {}"

#: src/util/error.rs:260
msgid "Transfer was not acknowledged by peer"
msgstr "Skráaflutningur var ekki samþykktur af jafningja"

#: src/util/error.rs:262
msgid "The received file is corrupted"
msgstr "Móttekin skrá er skemmd"

#: src/util/error.rs:268
msgid ""
"The file contained a different amount of bytes than advertised! Sent {} "
"bytes, but should have been {}"
msgstr ""
"Skráin er ekki í sömu stærð og tilkynnt var í bætum talið. Sendi {} bæti,en "
"hefði átt að vera {}"

#: src/util/error.rs:273
msgid "The other side has cancelled the transfer"
msgstr "Hinn aðilinn hætti við flutninginn"

#: src/util/error.rs:275
msgid "The other side has rejected the transfer"
msgstr "Hinn aðilinn hafnaði flutningnum"

#: src/util/error.rs:277
msgid "Something went wrong on the other side: {}"
msgstr "Einhvað fór úrskeiðis hjá hinum aðilanum:{}"

#: src/util/error.rs:292
msgid "Error while establishing file transfer connection"
msgstr "Villa við tengingu skráaflutnings."

#: src/util/error.rs:294
msgid "Unknown file transfer error"
msgstr "Óþekkt villa við flutning"

#: src/util/error.rs:303
msgid "An unknown error occurred while creating a zip file: {}"
msgstr "Óþekkt villa átti sér stað við að búa til zip-skrá: {}"

#: src/util/error.rs:304
msgid ""
"An unexpected error occurred. Please report an issue with the error message."
msgstr "Óvænt villa átti sér stað. Tilkynntu vandamálið með villumeldinguni."

#~ msgid "Save"
#~ msgstr "Vista"

#~ msgid "Back"
#~ msgstr "Til baka"

#~ msgid "Show in Folder"
#~ msgstr "Sýna í möppu"

#~ msgid "Accept File Transfer and save to Downloads folder"
#~ msgstr "Samþykkja skráaflutning og vista í niðurhalsmöppuna"

#~ msgid "Select Save Location"
#~ msgstr "Velja stað til að vista"

#~ msgid "Copy a detailed message for reporting an issue"
#~ msgstr "Afrita nákvæma villumeldingu fyrir bilanaskýrslu"

#~ msgid "Open"
#~ msgstr "Opna"

#~ msgid "Open Folder"
#~ msgstr "Opna möppu"
