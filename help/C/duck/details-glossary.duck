= Glossary
  [topic]
@link[guide >index#details]
@desc Definition of terms used through this help page

== Rendezvous Server
  [#rendezvous-server]
This server is located on the internet and is operated by the
$link[href=https://github.com/magic-wormhole/magic-wormhole](Magic Wormhole project). It is used to find peers to share
files with. When starting a transmission, both parties will contact the rendezvous server to find each other.

The rendezvous server also helps the transmitting client to generate a $link[>#transmit-code]

They will use this server to exchange cryptographic information for secure communication. Afterwards they will exchange
information about how to contact each other for file transfer.

[note]
    No files are transferred via the rendezvous server. See $link[>#transmit-relay] and $link[>#direct-transfer]

== Transmit Code
  [#transmit-code]
This code will be generated by the client in cooperation with the $link[>#rendezvous-server]. It is used to identify
and encrypt the transmission of messages and files.

To start a transmission you need a transmit code. The transmit code then needs to be communicated to the
receiver (preferably via an encrypted or otherwise secure channel). After entering the transmit code at the receiver
side the file transmission can begin.

[note]
    Every transmit code can only be entered once for security reasons.

== Transmit Link
  [#transmit-link]
It is also possible to encode the $link[>#transmit-code] as a link. Clicking the link will automatically open Warp and
start the file transmission. A $gui(Copy Transmit Link) button is available in the send screen. A transmit link looks
like this:

[code]
  wormhole-transfer:{code}

== Transmit Relay
  [#transmit-relay]
The transmit relay is also operated by the
$link[href=https://github.com/magic-wormhole/magic-wormhole](Magic Wormhole project)

It is used if no $link[>#direct-transfer](direct communication) between two peers can be established. This is most
commonly a problem when both parties are located in different home networks with
$link[href=https://en.wikipedia.org/wiki/Network_address_translation](NAT) or restrictive
$link[href=https://en.wikipedia.org/wiki/Firewall_(computing)](firewalls).

The files are transmitted via the transmit relay in an encrypted fashion. The relay will only know about the file size.

Transfers via the transmit relay may be slower than direct transfer, depending on relay congestion.

== Direct Transfer
  [#direct-transfer]
If both peers can find a direct networking path between each other they will send the file directly. This is often
referred to as a $link[href=https://en.wikipedia.org/wiki/Peer-to-peer](Peer-to-Peer) connection. This type of
connection is typically faster than a connection via the $link[>#transmit-relay]
