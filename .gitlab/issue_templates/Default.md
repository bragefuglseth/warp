<!--
Thank you for taking your time to submit an issue.

By submitting an issue you agree to follow the [GNOME Code of Conduct](https://wiki.gnome.org/Foundation/CodeOfConduct).

Before submitting an issue about a network problem, please check your own internet connection first. If the rendezvous server is unreachable, please try again some time later to allow for temporary server outages. It is also be possible that the server is blocked in your country.
-->

## Issue description

## Version information
<!-- eg. Version x.x.x from Flathub -->
